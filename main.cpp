#include<iostream>
#include<vector>
#include<queue>
#include<cmath>
using namespace std;

class Edge {
public:
	int s, t, f, c;
	int i;
};

int n, m;
vector<Edge> edges;
vector<vector<int>> head;
int startidx = 0;
int endidx;
int INF = 65535;
int cost = 0;
vector<int> dis;
vector<bool> vis;
vector<int> pre;

void addEdge(int s, int t, int f, int c) {
	Edge edge = { s,t,f,c,edges.size() + 1 };
	Edge other = { t,s,0,-c,edges.size() };
	head[s].push_back(edges.size());
	edges.push_back(edge);
	head[t].push_back(edges.size());
	edges.push_back(other);
}

bool spfa() {
	bool flag = false;
	dis = vector<int>(n * 7 + 2, INF);
	dis[startidx] = 0;
	vis = vector<bool>(n * 7 + 2, false);
	pre = vector<int>(n * 7 + 2, -1);

	queue<int> q;
	q.push(startidx);
	vis[startidx] = true;

	while (!q.empty()) {
		int now = q.front();
		q.pop();
		vis[now] = false;

		for (int i = 0; i < head[now].size(); i++) {
			int idx = head[now][i];
			Edge edge = edges[idx];

			if (edge.f > 0 && dis[now] + edge.c < dis[edge.t]) {
				dis[edge.t] = dis[now] + edge.c;
				pre[edge.t] = idx;

				if (edge.t == endidx) {
					flag = true;
				}

				if (!vis[edge.t]) {
					q.push(edge.t);
					vis[edge.t] = true;
				}
			}

		}
	}

	if (flag) {
		int flow = INF;
		int idx = pre[endidx];
		while (idx != -1) {
			Edge edge = edges[idx];
			flow = min(edge.f, flow);
			idx = pre[edge.s];
		}
		idx = pre[endidx];
		while (idx != -1) {
			Edge& edge = edges[idx];
			edge.f -= flow;
			edges[edge.i].f += flow;
			idx = pre[edge.s];
		}
	}

	return flag;
}

int main() {
	cin >> n >> m;
	head = vector<vector<int>>(n * 7 + 2);
	endidx = n * 7 + 1;

	int s, t, f, c;
	for (int i = 1; i <= n; i++) {
		int create;
		int use;
		for (int j = 1; j <= 7; j++) {
			cin >> create;
			if (create <= 0) {
				continue;
			}
			s = startidx;
			t = (j - 1) * n + i;
			f = create;
			c = 0;
			addEdge(s, t, f, c);
		}
		for (int j = 1; j <= 7; j++) {
			cin >> use;
			if (use <= 0) {
				continue;
			}
			s = (j - 1) * n + i;
			t = endidx;
			f = use;
			c = 0;
			addEdge(s, t, f, c);
		}
		int v, w;
		cin >> v >> w;
		for (int j = 1; j <= 7; j++) {
			s = (j - 1) * n + i;
			t = s + n;
			if (j == 7) {
				t = i;
			}
			f = v;
			c = w;
			addEdge(s, t, f, c);
		}
	}
	for (int i = 1; i <= m; i++) {
		int sk, tk, ck;
		cin >> sk >> tk >> ck;
		for (int j = 1; j <= 7; j++) {
			s = (j - 1) * n + sk;
			t = (j - 1) * n + tk;
			f = INF;
			c = ck;
			addEdge(s, t, f, c);
			addEdge(t, s, f, c);
		}
	}

	while (spfa());
	for (int i = 0; i < edges.size(); i++) {
		if (edges[i].c < 0) {
			cost -= edges[i].c * edges[i].f;
		}
	}
	cout << cost << endl;
	return 0;
}